# ETH Crawler



## Description
ETH Crawler allows user to view transaction data from the Ethereum blockchain associated with a specific wallet address.
Supported transaction types:
- Normal transactions
- ERC20 token transactions
- ERC721 (NFT) token transactions
- Blocks mined by address

## Requirements
- Java JRE 17
- Valid Etherscan.io API key

For testing purposes, API key is provided for you. However, for advanced features of program (Bonus points requirements), a valid Etherscan.io PRO API key is needed.
Update application.properties with PRO API key to enable Bonus points requirements (application.properties variable etherscan.api_key=).

## Usage
Compile and run, no special requirements.

- Backend: built with Java JRE 17 and Spring Boot (v2.6.7).
- Frontend: Thymeleaf and Bootstrap 5.
- Tests: JUnit 5

Deployed in browser: localhost:8081

Port can be adjusted in application.properties (variable server.port=)

Additional added dependencies:

[GSON](https://mvnrepository.com/artifact/com.google.code.gson/gson)

[OKHTTP3](https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp)

Not used, except for testing purposes while building:
[WEB3J](https://mvnrepository.com/artifact/org.web3j/core)

## Docker image
Optionaly, you can use Docker image:
[ETH Crawler docker image](https://hub.docker.com/repository/docker/risac/eth_crawler)

- To run application on port 8081, Start docker image with:

docker run -p 8081:8081 risac/eth_crawler

- To run application with a different Entherscan.io API Key (preferably, use PRO Api Key), pass variable:

docker run -e etherscan.api_key=API_KEY_HERE -p 8081:8081 risac/eth_crawler



## Features
### Index page:

![index](src/main/resources/static/pics/screenshots/index.jpg)

Input for wallet address uses regex checker, preventing user from inputing invalid wallet address.
Input for starting block query finds latest mined block and prevents user from inputing invalid block number.

### Wallet balance:

![data1](src/main/resources/static/pics/screenshots/data1.jpg)

### Bonus feature (untested):

NOTE! Requires Etherscan.io PRO API key!
Etherscan.io endpoints are locked behind paid feature.

![bonus](src/main/resources/static/pics/screenshots/bonus.jpg)

In case that user has only Etherscan.io Free API key, program will find and display closest block to selected date, which can be used on additional independent resources like [Etherscan Account Balance Checker](https://etherscan.io/balancecheck-tool) for checking.

In case of PRO API key, program should display correct data for tokens, but the feature is UNTESTED.

Note: newly generated API keys take several minutes to activate after creation and confirmation.

JUnit test src/test/java com.risac.entities.TestApiResponse is a good place to test the validity of newly generated API key.

### Ethereum transactions:

![data2](src/main/resources/static/pics/screenshots/data2.jpg)

### ERC20 transactions:

![data3](src/main/resources/static/pics/screenshots/data3.jpg)

### NFT transactions:

![data4](src/main/resources/static/pics/screenshots/data4.jpg)

## TODO

- Find better solution for API collection interrupt.

## Author

Damir Mikloš

damirmiklos@gmail.com
## Project status

Ready for deployment.

