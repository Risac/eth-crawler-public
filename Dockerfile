FROM openjdk:17
ADD target/ETHCrawler-0.0.1-SNAPSHOT.jar ETHCrawler-0.0.1-SNAPSHOT.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "ETHCrawler-0.0.1-SNAPSHOT.jar"]