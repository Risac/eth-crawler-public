package com.risac.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Class queries Etherscan endpoints, parses JSON response.
 *
 * @return Appropriate ApiResponse type.
 *
 * @param Etherscan.io URL in String form.
 */
@Service
public class Crawler {

	private static final Logger logger = LogManager.getLogger();
	boolean warningLogged = false;

	public Object getData(Class<?> clazz, String url) {

		Object response = null;
		Gson gson = new Gson();

		try {
			URL newUrl = new URL(url);
			URLConnection urlConnection = newUrl.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			response = gson.fromJson(bufferedReader, clazz);

			bufferedReader.close();

		} catch (JsonSyntaxException e) {
			// expected with non-Pro API key, log once
			if (!warningLogged) {
				logger.warn("Historical data for balances access requires Pro API key.");
			}
			warningLogged = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

}