package com.risac.controllers;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.risac.crawler.Crawler;
import com.risac.entities.ApiResponseBalance;
import com.risac.entities.ApiResponseBlockMined;
import com.risac.entities.ApiResponseBlockNum;
import com.risac.entities.ApiResponseBlocksMined;
import com.risac.entities.ApiResponseEth;
import com.risac.entities.ApiResponseEthTransaction;
import com.risac.entities.ApiResponseHistoricalBalance;
import com.risac.entities.ApiResponsePrice;
import com.risac.entities.ApiResponsePrices;
import com.risac.entities.ApiResponseTokenTransaction;
import com.risac.entities.ApiResponseTokens;
import com.risac.util.AddressValidator;
import com.risac.util.ControllerHelper;
import com.risac.util.TimeConverter;
import com.risac.util.URLBuilder;

@Controller
public class MainController {

	@Autowired
	private Crawler crawler;
	@Autowired
	private URLBuilder urlBuilder;
	@Autowired
	private ControllerHelper helper;
	@Autowired
	private AddressValidator validator;
	@Autowired
	private TimeConverter timeConverter;

	private static final Logger logger = LogManager.getLogger();

	private static List<ApiResponseEthTransaction> ethResult;
	private static List<ApiResponseTokenTransaction> erc20Result;
	private static List<ApiResponseTokenTransaction> erc721Result;
	private static List<ApiResponseBlockMined> minedResult;
	private static ApiResponsePrice priceResult;

	/*
	 * Maps index.html.
	 */
	@GetMapping(value = "/")
	public ModelAndView index(@RequestParam(required = false) String address,
			@RequestParam(required = false) String startingBlock, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		helper.passWarningMessage(session, modelAndView);

		try {
			BigInteger latest = helper.findLatestBlock();
			modelAndView.addObject("latest", latest);
		} catch (Exception e) {
			return new ModelAndView("redirect:/");
		}
		if (address != null && startingBlock != null) {
			return new ModelAndView("forward:/data"); // forward address and starting block to data page.
		}

		modelAndView.setViewName("index");
		return modelAndView;
	}

	@RequestMapping(value = "data")
	public ModelAndView data(@RequestParam(required = false) String address,
			@RequestParam(required = false) String startingBlock, @RequestParam(required = false) Date historicalDate,
			HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		address = address.trim(); // trim for trailing spaces, add to modelAndView
		modelAndView.addObject("address", address);

		boolean validAddress = validator.isValidAddress(address);
		if (!validAddress) {
			logger.warn("Incorrect wallet address.");
			String message = "* Check address format!";
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/");
		}

		Long timestamp = null;
		String historicalMessage = null;

		if (historicalDate != null) {
			try {
				timestamp = timeConverter.humanToEpoch(historicalDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		try {
			completeQueries(address, startingBlock, modelAndView);
		} catch (NullPointerException e) {
			logger.warn("Session interrupted by user.");
			String message = "Session interrupted, please allow API endpoints to collect data.";
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/");
		}
		/*
		 * Fetch URLs and query results for finding a block number and ETH and Token
		 * balances
		 */
		final StringBuilder blockByTimeURL = urlBuilder.createBlockByTimeURL(timestamp);
		ApiResponseBlockNum blockNum = null;

		StringBuilder historicalBalanceURL = null;
		ApiResponseHistoricalBalance historicalBalance = null;

		if (historicalDate != null) {
			StringBuilder historicalBalanceErc20URL = null;
			ApiResponseHistoricalBalance historicalBalanceErc20 = null;
			blockNum = (ApiResponseBlockNum) crawler.getData(ApiResponseBlockNum.class, blockByTimeURL.toString());
			// create URLS
			historicalBalanceURL = urlBuilder.createHistoricalBalanceETHURL(address, blockNum.getResult());
			historicalBalanceErc20URL = urlBuilder.createHistoricalBalanceErc20URL(address, address,
					blockNum.getResult());

			historicalMessage = helper.createHistoricalETHMessage(blockNum, historicalBalanceURL);
		}

		List<String> histMessages = helper.createHistoricalTokensMessage(address, historicalDate, blockNum,
				erc20Result);

		modelAndView.addObject("historicalMessage", historicalMessage);
		modelAndView.addObject("histMessages", histMessages);
		modelAndView.setViewName("data");
		return modelAndView;
	}

	/** builds URLs, fetches data and adds to Lists and modelAndView */
	public void completeQueries(String address, String startingBlock, ModelAndView modelAndView)
			throws NullPointerException {
		final StringBuilder balanceURL = urlBuilder.createBalanceURL(address);
		final ApiResponseBalance balanceData = (ApiResponseBalance) crawler.getData(ApiResponseBalance.class,
				balanceURL.toString());
		modelAndView.addObject("balanceData", balanceData);

		final StringBuilder ethURL = urlBuilder.createEthURL(address, startingBlock);
		final ApiResponseEth ethData = (ApiResponseEth) crawler.getData(ApiResponseEth.class, ethURL.toString());
		ethResult = ethData.getResult();
		modelAndView.addObject("ethResult", ethResult);

		final StringBuilder erc20URL = urlBuilder.createErc20URL(address, startingBlock);
		final ApiResponseTokens erc20Data = (ApiResponseTokens) crawler.getData(ApiResponseTokens.class,
				erc20URL.toString());
		erc20Result = erc20Data.getResult();
		modelAndView.addObject("erc20Result", erc20Result);

		final StringBuilder erc721URL = urlBuilder.createErc721URL(address, startingBlock);
		final ApiResponseTokens erc721Data = (ApiResponseTokens) crawler.getData(ApiResponseTokens.class,
				erc721URL.toString());
		erc721Result = erc721Data.getResult();
		modelAndView.addObject("erc721Result", erc721Result);

		final StringBuilder minedURL = urlBuilder.createMiningURL(address);
		final ApiResponseBlocksMined minedData = (ApiResponseBlocksMined) crawler.getData(ApiResponseBlocksMined.class,
				minedURL.toString());
		minedResult = minedData.getResult();
		modelAndView.addObject("minedResult", minedResult);

		final StringBuilder pricesURL = urlBuilder.createPriceURL(address);
		final ApiResponsePrices priceData = (ApiResponsePrices) crawler.getData(ApiResponsePrices.class,
				pricesURL.toString());
		priceResult = priceData.getResult();
		modelAndView.addObject("priceResult", priceResult);
	}

}
