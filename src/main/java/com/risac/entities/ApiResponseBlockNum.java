package com.risac.entities;

import java.math.BigInteger;

/**
 * Api Response for finding a block.
 */
public class ApiResponseBlockNum extends ApiResponse {

	private BigInteger result;

	public ApiResponseBlockNum(String status, String message, BigInteger result) {
		super(status, message);
		this.result = result;
	}

	public BigInteger getResult() {
		return result;
	}

	@Override
	public String toString() {
		return "BlockNum [getStatus()=" + getStatus() + ", getMessage()=" + getMessage() + ", result=" + result + "]";
	}

}
