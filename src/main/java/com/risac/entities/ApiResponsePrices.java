package com.risac.entities;

/**
 * Api Response for current Ethereum price.
 */
public class ApiResponsePrices extends ApiResponse {

	private ApiResponsePrice result;

	public ApiResponsePrices(String status, String message, ApiResponsePrice result) {
		super(status, message);
		this.result = result;
	}

	public ApiResponsePrice getResult() {
		return result;
	}

	@Override
	public String toString() {
		return "Prices [getStatus()=" + getStatus() + ", getMessage()=" + getMessage() + ", result=" + result + "]";
	}

}
