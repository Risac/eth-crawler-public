package com.risac.entities;

import java.util.List;

/**
 * Api Response for Ethereum tokens.
 */
public class ApiResponseEth extends ApiResponse {

	private List<ApiResponseEthTransaction> result;

	public ApiResponseEth(String status, String message, List<ApiResponseEthTransaction> result) {
		super(status, message);
		this.result = result;
	}

	public List<ApiResponseEthTransaction> getResult() {
		return result;
	}

	@Override
	public String toString() {
		return "ApiResponseEth [getStatus()=" + getStatus() + ", getMessage()=" + getMessage() + ", result=" + result
				+ "]";
	}

}
