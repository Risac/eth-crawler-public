package com.risac.entities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Api Response for an Erc20 token transaction.
 */
public class ApiResponseTokenTransaction extends ApiResponseEthTransaction {

	private String tokenName;
	private String tokenSymbol;
	private int tokenDecimal;
	private String contractAddress;

	public ApiResponseTokenTransaction(String blockNumber, Long timeStamp, String hash, String blockHash, String from, String to,
			BigDecimal value, BigInteger confirmations, BigInteger gasUsed, String tokenName, String tokenSymbol,
			int tokenDecimal, String contractAddress) {
		super(blockNumber, timeStamp, hash, blockHash, from, to, value, confirmations, gasUsed);
		this.tokenName = tokenName;
		this.tokenSymbol = tokenSymbol;
		this.tokenDecimal = tokenDecimal;
		this.contractAddress = contractAddress;
	}

	public String getTokenName() {
		return tokenName;
	}

	public String getTokenSymbol() {
		return tokenSymbol;
	}

	public double getTokenDecimal() {
		return tokenDecimal;
	}

	public String getContractAddress() {
		return contractAddress;
	}

	/**
	 * Calculates token values to proper decimal spot.
	 *
	 * @return BigDecimal representation of token value
	 */
	public BigDecimal getOwnValue() {
		Long res = (long) Math.pow(10.00, tokenDecimal);
		return getValue().divide(new BigDecimal(res));
	}

	@Override
	public int hashCode() {
		return Objects.hash(contractAddress, tokenDecimal, tokenName, tokenSymbol);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ApiResponseTokenTransaction other = (ApiResponseTokenTransaction) obj;
		return Objects.equals(contractAddress, other.contractAddress)
				&& Objects.equals(tokenDecimal, other.tokenDecimal) && Objects.equals(tokenName, other.tokenName)
				&& Objects.equals(tokenSymbol, other.tokenSymbol);
	}

	@Override
	public String toString() {
		return "TokenTransaction [getBlockNumber()=" + getBlockNumber() + ", getTimeStamp()=" + getTimeStamp()
				+ ", getTime()=" + getTime() + ", getHash()=" + getHash() + ", getBlockHash()=" + getBlockHash()
				+ ", getFrom()=" + getFrom() + ", getTo()=" + getTo() + ", getValue()=" + getValue()
				+ ", getEthValue()=" + getEthValue() + ", getConfirmations()=" + getConfirmations() + ", getGasUsed()="
				+ getGasUsed() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", tokenName=" + tokenName + ", tokenSymbol=" + tokenSymbol + ", tokenDecimal="
				+ tokenDecimal + ", contractAddress=" + contractAddress + "]";
	}

}
