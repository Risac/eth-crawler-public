package com.risac.entities;

import java.math.BigDecimal;

/**
 * Api Response for ETH wallet Balance.
 */
public class ApiResponseBalance extends ApiResponse {

	private BigDecimal result;

	public ApiResponseBalance(String status, String message, BigDecimal result) {
		super(status, message);
		this.result = result;
	}

	public BigDecimal getResult() {
		return result;
	}

	/**
	 * Calculates wei to ETH value. Formats trailing 0000 values to 0 for frontend.
	 *
	 * @return BigDecimal representation of ETH value
	 */
	public BigDecimal getEthValue() {
		if (result.compareTo(new BigDecimal("0")) == 0) {
			return new BigDecimal("0");
		}
		return result.divide(BigDecimal.valueOf(1000000000000000000.00));
	}

	@Override
	public String toString() {
		return "Balance [getStatus()=" + getStatus() + ", getMessage()=" + getMessage() + ", result=" + result + "]";
	}

}
