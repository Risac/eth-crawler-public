package com.risac.entities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Api Response for an Ethereum transaction.
 */
public class ApiResponseEthTransaction {

	private String blockNumber;
	private Long timeStamp;
	private String hash;
	private String blockHash;
	private String from;
	private String to;
	private BigDecimal value;
	private BigInteger confirmations;
	private BigInteger gasUsed;

	public ApiResponseEthTransaction(String blockNumber, Long timeStamp, String hash, String blockHash, String from, String to,
			BigDecimal value, BigInteger confirmations, BigInteger gasUsed) {
		super();
		this.blockNumber = blockNumber;
		this.timeStamp = timeStamp;
		this.hash = hash;
		this.blockHash = blockHash;
		this.from = from;
		this.to = to;
		this.value = value;
		this.confirmations = confirmations;
		this.gasUsed = gasUsed;
	}

	public String getBlockNumber() {
		return blockNumber;
	}

	public void setBlockNumber(String blockNumber) {
		this.blockNumber = blockNumber;
	}

	public Long getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Formats UNIX Epoch to readable form.
	 */
	public String getTime() {
		String time = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
				.format(new java.util.Date(timeStamp * 1000));
		return time;
	}

	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getBlockHash() {
		return blockHash;
	}

	public void setBlockHash(String blockHash) {
		this.blockHash = blockHash;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Calculates wei to ETH value. Formats trailing 0000 values to 0 for frontend.
	 *
	 * @return BigDecimal representation of ETH value
	 */
	public BigDecimal getEthValue() {
		if (value.compareTo(new BigDecimal("0")) == 0) {
			return new BigDecimal("0");
		}
		return value.divide(BigDecimal.valueOf(1000000000000000000.00));
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public BigInteger getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(BigInteger confirmations) {
		this.confirmations = confirmations;
	}

	public BigInteger getGasUsed() {
		return gasUsed;
	}

	public void setGasUsed(BigInteger gasUsed) {
		this.gasUsed = gasUsed;
	}

	@Override
	public int hashCode() {
		return Objects.hash(blockHash, blockNumber, confirmations, from, gasUsed, hash, timeStamp, to, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ApiResponseEthTransaction other = (ApiResponseEthTransaction) obj;
		return Objects.equals(blockHash, other.blockHash) && Objects.equals(blockNumber, other.blockNumber)
				&& Objects.equals(confirmations, other.confirmations) && Objects.equals(from, other.from)
				&& Objects.equals(gasUsed, other.gasUsed) && Objects.equals(hash, other.hash)
				&& Objects.equals(timeStamp, other.timeStamp) && Objects.equals(to, other.to)
				&& Objects.equals(value, other.value);
	}

	@Override
	public String toString() {
		return "NormalTransaction [blockNumber=" + blockNumber + ", timeStamp=" + timeStamp + ", hash=" + hash
				+ ", blockHash=" + blockHash + ", from=" + from + ", to=" + to + ", value=" + value + ", confirmations="
				+ confirmations + ", gasUsed=" + gasUsed + "]";
	}

}
