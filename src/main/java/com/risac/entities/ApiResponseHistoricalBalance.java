package com.risac.entities;

import java.math.BigInteger;

/**
 * Api Response for Ethereum balance in wallet on a specific block.
 */
public class ApiResponseHistoricalBalance extends ApiResponse {
	private BigInteger result;

	public ApiResponseHistoricalBalance(String status, String message, BigInteger result) {
		super(status, message);
		this.result = result;
	}

	public BigInteger getResult() {
		return result;
	}

	@Override
	public String toString() {
		return "HistoricalBalance [getStatus()=" + getStatus() + ", getMessage()=" + getMessage() + ", result=" + result
				+ "]";
	}

}
