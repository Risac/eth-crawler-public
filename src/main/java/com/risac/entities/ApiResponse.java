package com.risac.entities;

/**
 * Base class for Etherscan API response.
 */
public class ApiResponse {

	private String status;
	private String message;

	public ApiResponse(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

}
