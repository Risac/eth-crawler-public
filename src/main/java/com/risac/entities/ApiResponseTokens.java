package com.risac.entities;

import java.util.List;

/**
 * Api Response for Erc20 and Erc721 (NFT) tokens.
 */
public class ApiResponseTokens extends ApiResponse {

	private List<ApiResponseTokenTransaction> result;

	public ApiResponseTokens(String status, String message, List<ApiResponseTokenTransaction> result) {
		super(status, message);
		this.result = result;
	}

	public List<ApiResponseTokenTransaction> getResult() {
		return result;
	}

	@Override
	public String toString() {
		return "ApiResponseErc20 [getStatus()=" + getStatus() + ", getMessage()=" + getMessage() + ", result=" + result
				+ "]";
	}

}
