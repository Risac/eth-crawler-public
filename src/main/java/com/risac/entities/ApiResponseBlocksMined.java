package com.risac.entities;

import java.util.List;

/**
 * Api Response for list of blocks mined by wallet.
 */

public class ApiResponseBlocksMined extends ApiResponse {

	private List<ApiResponseBlockMined> result;

	public ApiResponseBlocksMined(String status, String message, List<ApiResponseBlockMined> result) {
		super(status, message);
		this.result = result;
	}

	public List<ApiResponseBlockMined> getResult() {
		return result;
	}

}
