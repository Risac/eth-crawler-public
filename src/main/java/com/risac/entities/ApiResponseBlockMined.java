package com.risac.entities;

import java.math.BigDecimal;

/**
 * Api Response for mined blocks.
 */
public class ApiResponseBlockMined {

	private String blockNumber;
	private Long timeStamp;
	private BigDecimal blockReward;

	public ApiResponseBlockMined(String blockNumber, Long timeStamp, BigDecimal blockReward) {
		super();
		this.blockNumber = blockNumber;
		this.timeStamp = timeStamp;
		this.blockReward = blockReward;
	}

	public String getBlockNumber() {
		return blockNumber;

	}

	public Long getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Calculates wei to ETH value.
	 *
	 * @return BigDecimal representation of ETH value
	 */
	public BigDecimal getBlockReward() {
		if (blockReward.compareTo(new BigDecimal("0")) == 0) {
			return new BigDecimal("0");
		}
		return blockReward.divide(BigDecimal.valueOf(1000000000000000000.00));
	}

	public String getTime() {
		String time = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
				.format(new java.util.Date(timeStamp * 1000));
		return time;
	}

	@Override
	public String toString() {
		return "BlockMined [blockNumber=" + blockNumber + ", timeStamp=" + timeStamp + ", blockReward=" + blockReward
				+ "]";
	}

}
