package com.risac.entities;

import java.math.BigDecimal;

/**
 * Api Response for current Ethereum value in BTC and $USD.
 */
public class ApiResponsePrice {

	private BigDecimal ethbtc;
	private BigDecimal ethusd;

	public ApiResponsePrice(BigDecimal ethbtc, BigDecimal ethusd) {
		super();
		this.ethbtc = ethbtc;
		this.ethusd = ethusd;
	}

	public BigDecimal getEthbtc() {
		return ethbtc;
	}

	public BigDecimal getEthusd() {
		return ethusd;
	}

	@Override
	public String toString() {
		return "Price [ethbtc=" + ethbtc + ", ethusd=" + ethusd + "]";
	}

}
