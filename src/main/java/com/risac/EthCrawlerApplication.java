package com.risac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EthCrawlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EthCrawlerApplication.class, args);

	}

}
