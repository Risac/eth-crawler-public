package com.risac.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

/**
 * Custom built util class for converting UNIX Epoch to readable form and vice
 * versa.
 */
@Service
public class TimeConverter {

	public Date epochToHuman(Long epoch) {
		Date date = new Date(epoch * 1000L);
		return date;
	}

	public Long humanToEpoch(Date historicalDate) throws ParseException {
		String formatted = new SimpleDateFormat("MM-dd-yyyy HH:mm").format(historicalDate);
		SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm");
		Date date = df.parse(formatted);

		long epoch = date.getTime() / 1000;
		return epoch;
	}

}
