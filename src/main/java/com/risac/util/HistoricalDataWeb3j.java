package com.risac.util;

/**
 * NOT UTILISED. Class was used for testing Web3j approach to resolving
 * blockchain queries with INFURA API.
 *
 * @returnsEthereum balance in wallet on a specific block.
 */
public class HistoricalDataWeb3j {

//	@Autowired
//	static PrivateConfig priv;
//
//	public static BigDecimal getHistoricalData(String address, String startingBlock)
//			throws ExecutionException, InterruptedException, TimeoutException {
//
//		Web3j client = Web3j.build(new HttpService(priv.INFURA_API_KEY));
//		EthGetBalance response = null;
//		BigDecimal ethresult = null;
//		BigInteger block = new BigInteger(startingBlock);
//
//		response = client.ethGetBalance(address, new DefaultBlockParameterNumber(block)).sendAsync().get(10,
//				TimeUnit.SECONDS);
//
//		BigInteger result = response.getBalance();
//		BigDecimal res = new BigDecimal(result);
//		ethresult = res.divide(BigDecimal.valueOf(1000000000000000000.00));
//		return ethresult;
//	}

}
