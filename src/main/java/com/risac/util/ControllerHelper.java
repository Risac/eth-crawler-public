package com.risac.util;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import com.risac.crawler.Crawler;
import com.risac.entities.ApiResponseBlockNum;
import com.risac.entities.ApiResponseHistoricalBalance;
import com.risac.entities.ApiResponseTokenTransaction;

@Component
public class ControllerHelper {

	@Autowired
	private URLBuilder urlBuilder;
	@Autowired
	private Crawler crawler;

	/**
	 * Finds latest block in ETH blockchain.
	 */
	public BigInteger findLatestBlock() {
		LocalDateTime now = LocalDateTime.now(ZoneId.systemDefault());
		long epochSecond = now.toEpochSecond(ZoneOffset.ofHours(2));

		StringBuilder blockURL = urlBuilder.createBlockByTimeURL(epochSecond);
		ApiResponseBlockNum latestBlock = (ApiResponseBlockNum) crawler.getData(ApiResponseBlockNum.class,
				blockURL.toString());

		BigInteger latest = latestBlock.getResult();

		return latest;
	}

	/**
	 * Create message for ETH value frontend display.
	 */
	public String createHistoricalETHMessage(ApiResponseBlockNum blockNum, StringBuilder historicalBalanceURL) {
		String historicalMessage;
		ApiResponseHistoricalBalance historicalBalance;
		try {
			historicalBalance = (ApiResponseHistoricalBalance) crawler.getData(ApiResponseHistoricalBalance.class,
					historicalBalanceURL.toString());
			BigInteger result = historicalBalance.getResult();

			if (historicalBalance.getResult().compareTo(new BigInteger("0")) == 0) { // resolve long zero values
				result = new BigInteger("0");
			}

			// set messages for frontend display.
			historicalMessage = "Closest block:" + blockNum.getResult() + ". Balance on selected date is: " + result
					+ " ETH";
		} catch (Exception e) {
			historicalMessage = "Your Free Etherscan API key does not support Historical Data. Closest block to selected date is: "
					+ blockNum.getResult();
		}
		return historicalMessage;
	}

	/**
	 * Collects a List of Token values for specific block number.
	 *
	 * @param erc20Result
	 */
	public List<String> createHistoricalTokensMessage(String address, Date historicalDate, ApiResponseBlockNum blockNum,
			List<ApiResponseTokenTransaction> erc20Result) {
		StringBuilder historicalBalanceErc20URL;
		ApiResponseHistoricalBalance historicalBalanceErc20;
		Map<String, String> contractAddresses = new HashMap<>();
		Map<String, BigInteger> tokens = new HashMap<>();

		// collect token name and contract address pair
		for (ApiResponseTokenTransaction t : erc20Result) {
			if (!contractAddresses.containsValue(t.getContractAddress())) {
				contractAddresses.put(t.getTokenName(), t.getContractAddress());
			}
		}

		List<String> histMessages = new ArrayList<>();
		if (historicalDate != null) {
			// for each token name, find value at specific block number
			for (Map.Entry<String, String> entry : contractAddresses.entrySet()) {
				String mess = "";

				historicalBalanceErc20URL = urlBuilder.createHistoricalBalanceErc20URL(entry.getValue(), address,
						blockNum.getResult());

				historicalBalanceErc20 = (ApiResponseHistoricalBalance) crawler
						.getData(ApiResponseHistoricalBalance.class, historicalBalanceErc20URL.toString());

				// collect String messages for frontend display.
				if (historicalBalanceErc20 != null) {
					mess = "Value of " + entry.getKey() + " was " + historicalBalanceErc20.getResult();
					tokens.put(entry.getKey(), historicalBalanceErc20.getResult());
					histMessages.add(mess);
				}

			}
		}
		return histMessages;
	}

	/**
	 * Passes warning message from Http session and nulls it after adding object to
	 * model.
	 */
	public void passWarningMessage(HttpSession session, ModelAndView modelAndView) {
		if (session.getAttribute("message") != null) {
			Object attribute = session.getAttribute("message");
			modelAndView.addObject("warningMessage", attribute.toString());
			session.setAttribute("message", null);
		}
	}

}
