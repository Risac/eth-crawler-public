package com.risac.util;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.risac.privateconfig.PrivateConfig;

/**
 * Util class for creating URLs for connecting to Etherscan end-point.
 *
 * @return StringBuilder with formed URL.
 * @param wallet address, starting query block, timestamp, contract address.
 */

@Service
public class URLBuilder {
	@Autowired
	PrivateConfig priv;
	private static final String BASE_URL = "https://api.etherscan.io/api?module=";

	public StringBuilder createEthURL(String address, String startingBlock) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("account&action=txlist&address=");
		addVariables(address, startingBlock, sb);

		return sb;
	}

	public StringBuilder createErc20URL(String address, String startingBlock) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("account&action=tokentx&address=");
		addVariables(address, startingBlock, sb);

		return sb;

	}

	public StringBuilder createErc721URL(String address, String startingBlock) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("account&action=tokennfttx&address=");
		addVariables(address, startingBlock, sb);

		return sb;
	}

	public StringBuilder createBalanceURL(String address) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("account&action=balance&address=");
		sb.append(address);
		sb.append("&tag=latest&apikey=");
		sb.append(priv.API_KEY);

		return sb;
	}

	public StringBuilder createMiningURL(String address) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("account&action=getminedblocks&address=");
		sb.append(address);
		sb.append("&blocktype=blocks");
		sb.append(priv.API_KEY);

		return sb;
	}

	public StringBuilder createPriceURL(String address) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("stats&action=ethprice&apikey=");
		sb.append(priv.API_KEY);

		return sb;
	}

	public StringBuilder createBlockByTimeURL(Long timestamp) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("block&action=getblocknobytime&timestamp=");
		sb.append(timestamp);
		sb.append("&closest=before&apikey=");
		sb.append(priv.API_KEY);

		return sb;
	}

	public StringBuilder createHistoricalBalanceETHURL(String address, BigInteger blockNum) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("account&action=balancehistory&address=");
		sb.append(address);
		sb.append("&blockno=");
		sb.append(blockNum);
		sb.append("&apikey=");
		sb.append(priv.API_KEY);

		return sb;
	}

	public StringBuilder createHistoricalBalanceErc20URL(String contractAddress, String address, BigInteger blockNum) {
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_URL);
		sb.append("account&action=tokenbalancehistory&contractaddress=");
		sb.append(contractAddress);
		sb.append("&address=");
		sb.append(address);
		sb.append("&blockno=");
		sb.append(blockNum);
		sb.append("&apikey=");
		sb.append(priv.API_KEY);

		return sb;
	}

	public StringBuilder addVariables(String address, String startingBlock, StringBuilder sb) {
		sb.append(address);
		sb.append("&startblock=");
		sb.append(startingBlock);
		sb.append("&sort=asc&apikey=");
		sb.append(priv.API_KEY);

		return sb;
	}

}
