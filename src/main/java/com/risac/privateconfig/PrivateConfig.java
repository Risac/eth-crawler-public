package com.risac.privateconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class PrivateConfig {

	@Value("${infura.api_key}")
	public String INFURA_API_KEY;

	@Value("${etherscan.api_key}")
	public String API_KEY;

}
