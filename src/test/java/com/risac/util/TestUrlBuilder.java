package com.risac.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.risac.privateconfig.PrivateConfig;

@SpringBootTest
public class TestUrlBuilder {

	@Autowired
	URLBuilder urlBuilder;
	@Autowired
	PrivateConfig priv;

	@Test
	public void testURLBuilder() {
		String address = "xxx";
		String startingBLock = "100";
		String api = priv.API_KEY;

		StringBuilder url = urlBuilder.createEthURL(address, startingBLock);
		assertNotNull(url);
		String s = "https://api.etherscan.io/api?module=account&action=txlist&address=xxx&startblock=100&sort=asc&apikey="
				+ api;
		assertEquals(s, url.toString());

		url = urlBuilder.createErc20URL(address, startingBLock);
		assertNotNull(url);
		s = "https://api.etherscan.io/api?module=account&action=tokentx&address=xxx&startblock=100&sort=asc&apikey="
				+ api;
		assertEquals(s, url.toString());

		url = urlBuilder.createBalanceURL(address);
		assertNotNull(url);
		s = "https://api.etherscan.io/api?module=account&action=balance&address=xxx&tag=latest&apikey=" + api;
		assertEquals(s, url.toString());

		url = urlBuilder.createErc721URL(address, startingBLock);
		assertNotNull(url);
		s = "https://api.etherscan.io/api?module=account&action=tokennfttx&address=xxx&startblock=100&sort=asc&apikey="
				+ api;
		assertEquals(s, url.toString());

		url = urlBuilder.createMiningURL(address);
		assertNotNull(url);
		s = "https://api.etherscan.io/api?module=account&action=getminedblocks&address=xxx&blocktype=blocks" + api;
		assertEquals(s, url.toString());

		url = urlBuilder.createPriceURL(address);
		assertNotNull(url);
		s = "https://api.etherscan.io/api?module=stats&action=ethprice&apikey=" + api;
		assertEquals(s, url.toString());

		url = urlBuilder.createBlockByTimeURL(null);
		assertNotNull(url);
		s = "https://api.etherscan.io/api?module=block&action=getblocknobytime&timestamp=null&closest=before&apikey="
				+ api;
		assertEquals(s, url.toString());

		url = urlBuilder.addVariables(address, startingBLock, url);
		assertNotNull(url);
		s = "https://api.etherscan.io/api?module=block&action=getblocknobytime&timestamp=null&closest=before&apikey="
				+ api + "xxx&startblock=100&sort=asc&apikey=" + api;
		assertEquals(s, url.toString());

	}
}
