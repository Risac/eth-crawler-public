package com.risac.util;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AddressValidator.class)
public class TestAddressValidator {

	@Autowired
	private AddressValidator validator;

	@Test
	public void testValidator() {
		String address = "0x0BB9483a55Bf2820e7a25787438ee4319A8210b0";
		boolean validAddress = validator.isValidAddress(address);
		assertTrue(validAddress);

		address = "0BB9483a55Bf2820e7a25787438ee4319A8210b0";
		validAddress = validator.isValidAddress(address);
		assertFalse(validAddress);

		address = "0x000000000000000000000000000000";
		validAddress = validator.isValidAddress(address);
		assertFalse(validAddress);
	}

}
