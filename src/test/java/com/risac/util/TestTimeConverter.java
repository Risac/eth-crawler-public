package com.risac.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = TimeConverter.class)
public class TestTimeConverter {

	@Autowired
	private TimeConverter timeConverter;

	@Test
	public void testEpochToHuman() {
		Long epoch = 1652307929L;
		Date date = timeConverter.epochToHuman(epoch);
		String test = "Thu May 12 00:25:29 CEST 2022";

		assertEquals(test, date.toString());

	}

	@Test
	public void testHumanToEpoch() throws ParseException {

		Long epoch = 1652307900L;
		Date date = timeConverter.epochToHuman(epoch);
		Long humanToEpoch = timeConverter.humanToEpoch(date);

		assertEquals(epoch, humanToEpoch);

	}
}
