package com.risac.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.risac.crawler.Crawler;
import com.risac.util.URLBuilder;

/**
 * Test expected responses from Etherscan API.
 */
@SpringBootTest
public class TestApiResponse {

	@Autowired
	URLBuilder urlBuilder;
	@Autowired
	Crawler crawler;

	@Test
	public void testApi() {

		String address = "0xaa7a9ca87d3694b5755f213b5d04094b8d0f0a6f";
		String startingBlock = "9000000";
		String OKstatus = "1";
		String OKmessage = "OK";

		StringBuilder balanceURL = urlBuilder.createBalanceURL(address);
		ApiResponseBalance balanceData = (ApiResponseBalance) crawler.getData(ApiResponseBalance.class,
				balanceURL.toString());
		String status = balanceData.getStatus();
		String message = balanceData.getMessage();
		assertEquals(status, OKstatus);
		assertEquals(message, OKmessage);

		StringBuilder ethURL = urlBuilder.createEthURL(address, startingBlock);
		ApiResponseEth ethData = (ApiResponseEth) crawler.getData(ApiResponseEth.class, ethURL.toString());
		status = ethData.getStatus();
		message = ethData.getMessage();
		assertEquals(status, OKstatus);
		assertEquals(message, OKmessage);

		StringBuilder erc20URL = urlBuilder.createErc20URL(address, startingBlock);
		ApiResponseTokens erc20Data = (ApiResponseTokens) crawler.getData(ApiResponseTokens.class, erc20URL.toString());
		status = erc20Data.getStatus();
		message = erc20Data.getMessage();
		assertEquals(status, OKstatus);
		assertEquals(message, OKmessage);

		StringBuilder erc721URL = urlBuilder.createErc721URL(address, startingBlock);
		ApiResponseTokens erc721Data = (ApiResponseTokens) crawler.getData(ApiResponseTokens.class,
				erc721URL.toString());
		status = erc721Data.getStatus();
		message = erc721Data.getMessage();
		assertEquals(status, "0");
		assertEquals(message, "No transactions found");

		StringBuilder minedURL = urlBuilder.createMiningURL(address);
		ApiResponseBlocksMined minedData = (ApiResponseBlocksMined) crawler.getData(ApiResponseBlocksMined.class,
				minedURL.toString());
		status = minedData.getStatus();
		message = minedData.getMessage();
		assertEquals(status, "0");
		assertEquals(message, "No transactions found");

		StringBuilder pricesURL = urlBuilder.createPriceURL(address);
		ApiResponsePrices priceData = (ApiResponsePrices) crawler.getData(ApiResponsePrices.class,
				pricesURL.toString());
		status = priceData.getStatus();
		message = priceData.getMessage();
		assertEquals(status, OKstatus);
		assertEquals(message, OKmessage);
	}

}
